const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    specPattern: [
      "cypress/e2e/voting_service.tests.cy.js",
      "cypress/e2e/result_service.tests.cy.js",
      "cypress/e2e/voting_api.tests.cy.js",
      "cypress/e2e/integrate.voting.results.tests.cy.js"
    ]
  },
});
