describe('integratevotingResults.cy.js', () => {
    var a = 0;
    var b = 0;
    var voteCookie;

    function setVotes(){
        cy.get('#result > .ng-binding')
        cy.window().then((win) => {
            cy.log('a',a)
            cy.log('b',b)
            a = win.data['a']
            b = win.data['b']
            cy.log('a',a)
            cy.log('b',b)
           })
           
    }
    it('load vote site', () => {
        cy.visit('http://10.7.4.102:31711/')
        cy.get('#result > .ng-binding')

        //cy.wait(1000)
      })

    it('Get curent votes',()=>{
        setVotes()
    })

    it('vote for dog',() => {
      let dogVotes = 0;
        cy.request({
          method: 'POST',
          url: 'http://10.7.4.102:32491/', // baseUrl is prepend to URL
          form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
          body: {
            vote: 'b'
          }
        }).then((response) => {
          expect(response.status).to.eq(200)
          expect(response.body).to.contain("(Tip: you can change your vote)")
        }).then(() => {
          cy.getCookie('voter_id')
          .should('exist')
          .then((c) => {
          // save cookie until we need it
          voteCookie = c['value']
          })
          dogVotes = b;
          cy.reload()
          setVotes()
        }).then(() =>{
          assert.equal(dogVotes+1,b)
        });
    })

    it('non cat or dog vote using valid POST call',() => {
      let catVotes = a
      let dogVotes = b
      cy.request({
        method: 'POST',
        url: 'http://10.7.4.102:32491/', // baseUrl is prepend to URL
        form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
        body: {
          vote: 'd'
        }
      }).then((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.contain("(Tip: you can change your vote)")
      }).then(() => {
        cy.reload()
        setVotes()
      }).then(() =>{
        assert(catVotes==a && dogVotes == b)
      })
  
    })

    it('verify changing vote',() => {
      let catVotes = a
      let dogVotes = b
      cy.setCookie('voter_id',voteCookie).request({
        method: 'POST',
        url: 'http://10.7.4.102:32491/', // baseUrl is prepend to URL
        form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
        body: {
          vote: 'a'
        }
      }).then((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.contain("(Tip: you can change your vote)")
      }).then(() => {
        cy.reload()
        setVotes()
      }).then(() =>{
        assert(catVotes+1==a && dogVotes-1 == b)
      })

    })

    it('verify invalid vote removes cat vote',() => {

      let catVotes = a
      let dogVotes = b
      cy.log(a)
      cy.setCookie('voter_id',voteCookie).request({
        method: 'POST',
        url: 'http://10.7.4.102:32491/', // baseUrl is prepend to URL
        form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
        body: {
          vote: 'd'
        }
      }).then((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.contain("(Tip: you can change your vote)")
      }).then(() => {
        cy.reload()
        setVotes()
      }).then(() =>{
        assert(catVotes-1 ==a && dogVotes == b)
      })

    })

});