describe('resultService.cy.js', () => {

    it('load vote site', () => {
      cy.visit('http://10.7.4.102:31711/')
    })
    it('verify elements', () => {
      cy.get('#choice')
      cy.window().then((win) => {
        var name = win['app']['name']
        assert.equal(name,'catsvsdogs')
      })
      cy.get('#result > .ng-binding').then(($span) => {
        var reg = /\d+/;
        const votes = $span.text();
        cy.log(votes.match(reg))
      })
      //cy.screenshot()
      
    })
    
    it('verify percent function',() => {
      cy.window().invoke('getPercentages',2,3).then(result => {
        expect(result['a']).to.eq(40)
        expect(result['b']).to.eq(60)
      })
      cy.window().invoke('getPercentages',40.9,69.9).then(result => {
        expect(result['a']).to.eq(37)
        expect(result['b']).to.eq(63)
      })
      cy.window().invoke('getPercentages',2,-3).then(result => {
        expect(result['a']).to.eq(50)
        expect(result['b']).to.eq(50)
      })
    })



})