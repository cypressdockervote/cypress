//const {NodeSSH} = require('node-ssh')
describe('voting.cy.js', () => {
  //const ssh = new NodeSSH()

  // before((done) => {
  //   ssh.connect({
  //     host: '10.7.4.102',
  //     username: 'chris',
  //     privateKey: '/home/chris/.ssh/id_rsa'
  //   })
  //   ssh.execCommand("microk8s kubectl delete -k /home/chris/examples/webserver/voting-webapp/application/").then(function(result) {
  //     //verify result has following deleted 
  //   /*service "db" deleted
  //     service "redis" deleted
  //     service "result-service" deleted
  //     service "voting-service" deleted
  //     deployment.apps "db" deleted
  //     deployment.apps "redis" deleted
  //     deployment.apps "result-service" deleted
  //     deployment.apps "voting-service" deleted
  //     deployment.apps "worker" deleted */
      
  //   })
  //   ssh.execCommand("microk8s kubectl apply -k /home/chris/examples/webserver/voting-webapp/application/").then(function(result) {
  //     //verify result is 
  //     /* service/db created
  //     service/redis created
  //     service/result-service created
  //     service/voting-service created
  //     deployment.apps/db created
  //     deployment.apps/redis created
  //     deployment.apps/result-service created
  //     deployment.apps/voting-service created
  //     deployment.apps/worker created */
  //   })

  // });

  it('load vote site', () => {
    cy.visit('http://10.7.4.102:32491/')
  })
  it('verify elements', () => {
    cy.title().should('include', 'Cats vs Dogs!')
    cy.get('#b')
    cy.get('#a')

    
  })
  it('vote for dog',() =>{
    cy.get('#b').click();
    cy.get('#b').find('.fa')
    cy.get('#a').find('.fa').should('not.exist')

  })

  it('switch vote to cat',() =>{
    cy.get('#a').click();
    cy.get('#a').find('.fa')
    cy.get('#b').find('.fa').should('not.exist')
  })

  it('verify votes', () =>{
  })

})
