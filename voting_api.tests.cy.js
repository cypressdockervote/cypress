describe('votingApi.cy.js', () => {
    it('vote using valid POST call',() => {
        cy.visit('http://10.7.4.102:32491/')//Need as not set as baseUrl for e2e
        cy.request({
          method: 'POST',
          url: '/', // baseUrl is prepend to URL
          form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
          body: {
            vote: 'b'
          }
        }).then((response) => {
          expect(response.status).to.eq(200)
          expect(response.body).to.contain("(Tip: you can change your vote)")
        })
    
      })

      it('vote using invalid form POST call',() => {
        cy.request({
          method: 'POST',
          url: '/', // baseUrl is prepend to URL
          form: false, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
          body: {
            vote: 'b'
          },
          failOnStatusCode: false
          
        }).then((response) => {
          expect(response.status).to.not.equal(200)
          expect(response.body).to.contain("Bad Request")
        })
    
      })

      it('non cat or dog vote using valid POST call',() => {
        cy.visit('http://10.7.4.102:32491/')//Need as not set as baseUrl for e2e
        cy.request({
          method: 'POST',
          url: '/', // baseUrl is prepend to URL
          form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
          body: {
            vote: 'd'
          }
        }).then((response) => {
          expect(response.status).to.eq(200)
          expect(response.body).to.contain("(Tip: you can change your vote)")
        })
    
      })

})